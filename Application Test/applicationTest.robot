*** Settings ***
Resource  ../Resource.Robot

Documentation  this is to test basic logins with a target application

Suite Setup  Open Application Under Test  ${Application}
Suite Teardown  Close Application

*** Variables ***
${Application}  com.android.commonwealthbank

${Valid_Username}  rob.staples91
${Valid_Password}  nottoday

${Invalid_Username}  fakename
${Invalid_Password}  killthepriest

${Username_Space_X}  45
${Username_Space_Y}  45

${Password_Space_X}  45
${Password_Space_Y}  45

${LoginButton_Space_X}  45
${LoginButton_Space_Y}  45
*** Test Cases ***
Valid Login
    login to application  ${Valid_Username}  ${Valid_Password}  ${Username_Space_X}  ${Username_Space_Y}  ${Password_Space_X}  ${Password_Space_Y}  ${LoginButton_Space_X}  ${LoginButton_Space_Y}
Invalid Login
    login to application  ${Invalid_Username}  ${Invalid_Password}  ${Username_Space_X}  ${Username_Space_Y}  ${Password_Space_X}  ${Password_Space_Y}  ${LoginButton_Space_X}  ${LoginButton_Space_Y}