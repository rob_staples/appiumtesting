*** Settings ***
Resource  ../Resource.Robot

Suite Setup  Open Application Under Test  ${Application}
Suite Teardown  Close Application

*** Variables ***
${Application}  com.android.browser
${Site}  www.
${Search_Phrase_1}  robotframework
${Search_Phrase_2}  Appium
*** Test Cases ***
Search for valid Github search
    Search Github  ${search_phrase_1}

Search for valid Github search
    Open Application Under Test  ${Application}
    Search Searchcode  ${search_phrase_2}